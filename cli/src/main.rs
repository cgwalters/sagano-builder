use anyhow::{anyhow, Context, Result};
use camino::Utf8Path;
use cap_std::fs::Dir;
use cap_std_ext::{cap_std, prelude::CapStdExtDirExt};
use clap::Parser;
use fn_error_context::context;
use xshell::cmd;

mod ocidir;
mod srcconfig;

/// Yes we're using the CI configuration as canonical
const CONFIG_SRC: &str = ".gitlab-ci.yml";
const SB_INFO: &str = "sagano-builddir.json";
const SRCDIR: &str = "src";
const CACHEDIR: &str = "cache";
const OCIDIR: &str = "oci";
const TMPDIR: &str = "tmp";

#[derive(serde::Serialize, serde::Deserialize)]
struct BuilderInfo {
    version: u32,
}

/// Toplevel options for extended ostree functionality.
#[derive(Debug, Parser)]
#[clap(name = "sagano-builder")]
#[clap(rename_all = "kebab-case")]
pub(crate) enum Opt {
    /// Initialize build directory, cloning target git repository
    Init(InitOpts),
    /// Remove local builds (and optionally caches)
    Clean(CleanOpts),
    /// Perform a build
    Build(BuildOpts),
    /// Print information
    Status(StatusOpts),
}

/// Options for initialization
#[derive(Debug, clap::Parser)]
pub(crate) struct InitOpts {
    /// URL to target git repository
    repo: String,
}

/// Options for cleaning
#[derive(Debug, clap::Parser)]
pub(crate) struct CleanOpts {
    /// Also remove the cached data
    #[clap(long)]
    all: bool,
}

/// Options for building
#[derive(Debug, clap::Parser)]
pub(crate) struct BuildOpts {}

/// Options for status
#[derive(Debug, clap::Parser)]
pub(crate) struct StatusOpts {}

/// An initialized builder working root
struct WorkingDirectory {
    sh: xshell::Shell,
    #[allow(dead_code)]
    buildmeta: BuilderInfo,
    #[allow(dead_code)]
    path: camino::Utf8PathBuf,
    dir: Dir,
    ocid: ocidir::OciDir,

    config: srcconfig::CiConfiguration,
}

#[allow(dead_code)]
fn target_to_tag(s: &str) -> Result<String> {
    let (prefix, version) = s
        .rsplit_once('-')
        .ok_or_else(|| anyhow!("Invalid target: {s}"))?;
    Ok(format!("{prefix}:{version}"))
}

#[context("Initializing")]
fn init(opts: InitOpts) -> Result<()> {
    let sh = xshell::Shell::new()?;
    let repo = opts.repo;

    if Utf8Path::new(SB_INFO).try_exists()? {
        anyhow::bail!("{SB_INFO} already exists");
    }

    let root = Dir::open_ambient_dir(".", cap_std::ambient_authority())?;
    root.atomic_replace_with(SB_INFO, |w| {
        serde_json::to_writer(w, &BuilderInfo { version: 1 }).map_err(anyhow::Error::msg)
    })?;

    if Utf8Path::new(SRCDIR).try_exists()? {
        anyhow::bail!("Source directory {SRCDIR} already present");
    }
    cmd!(sh, "git clone --recurse-submodules {repo} {SRCDIR}").run()?;

    for d in [CACHEDIR, TMPDIR, OCIDIR] {
        root.create_dir(d)?;
    }
    ocidir::OciDir::create(&root.open_dir(OCIDIR)?)?;

    Ok(())
}

fn open() -> Result<WorkingDirectory> {
    let dir = Dir::open_ambient_dir(".", cap_std::ambient_authority())?;
    let path = std::env::current_dir()?.try_into()?;

    let buildmeta = dir
        .open_optional(SB_INFO)?
        .ok_or_else(|| anyhow!("{SB_INFO} not present; not a builder working directory?"))
        .map(std::io::BufReader::new)?;
    let buildmeta = serde_json::from_reader(buildmeta)?;

    let ocid = dir.open_dir(OCIDIR)?;
    let ocid = ocidir::OciDir::open(&ocid)?;

    let configsrc = format!("{SRCDIR}/{CONFIG_SRC}");
    let config = dir.open(&configsrc).map(std::io::BufReader::new)?;
    let config = serde_yaml::from_reader(config).with_context(|| format!("Parsing {configsrc}"))?;

    let sh = xshell::Shell::new()?;

    Ok(WorkingDirectory {
        sh,
        buildmeta,
        dir,
        ocid,
        path,
        config,
    })
}

#[context("Cleaning")]
fn clean(opts: CleanOpts) -> Result<()> {
    let wd = open()?;

    wd.ocid.clean()?;

    if opts.all {
        wd.dir.remove_all_optional(CACHEDIR)?;
        wd.dir.create_dir(CACHEDIR)?;
    }

    Ok(())
}

#[context("Building")]
fn build(_opts: BuildOpts) -> Result<()> {
    let wd = open()?;

    let is_empty = {
        let idx = wd.ocid.read_index()?;
        idx.manifests().len() == 0
    };
    // TODO update builder to git rpm-ostree and use --initialize-mode=if-not-exists
    let composeargs = is_empty.then_some("--initialize");

    let target = wd.config.default();
    println!("Building target: {target}");
    wd.sh.create_dir(format!("{CACHEDIR}/rpm-ostree"))?;
    // let tag = target_to_tag(target)?;
    cmd!(
        wd.sh,
        "sudo rpm-ostree compose image --cachedir={CACHEDIR}/rpm-ostree {composeargs...} --format=oci {SRCDIR}/{target}.yaml {OCIDIR}"
    )
    .run()?;

    Ok(())
}

#[context("Status")]
fn status(_opts: StatusOpts) -> Result<()> {
    let wd = open()?;

    let targets = wd.config.expand();
    println!("Targets:");
    for t in targets {
        println!("  {t}");
    }

    let index = wd.ocid.read_index()?;
    match index.manifests().as_slice() {
        [] => {
            println!("No container builds.");
        }
        [o] => {
            let manifest: oci_spec::image::ImageManifest = wd.ocid.read_json_blob(o)?;
            let config = manifest.config();
            let config: oci_spec::image::ImageConfiguration = wd.ocid.read_json_blob(config)?;
            let version = config.version().unwrap_or("<no version set>");
            println!("Container build: {version}");
        }
        n => {
            println!("Container builds: {}", n.len());
        }
    }

    Ok(())
}

fn run() -> Result<()> {
    tracing_subscriber::fmt::init();

    match Opt::parse() {
        Opt::Init(opts) => init(opts),
        Opt::Clean(opts) => clean(opts),
        Opt::Build(opts) => build(opts),
        Opt::Status(opts) => status(opts),
    }
}

fn main() {
    if let Err(e) = run() {
        eprintln!("error: {:#}", e);
        std::process::exit(1);
    }
}

#[test]
fn test_target_tag() {
    assert_eq!(
        target_to_tag("fedora-tier-1-38").unwrap(),
        "fedora-tier-1:38"
    )
}
