use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "UPPERCASE")]
struct Config {
    tier: Vec<String>,
    os: String,
    version: Vec<String>,
    variant: Vec<String>,
}

#[derive(Deserialize)]
struct Parallel {
    matrix: Vec<Config>,
}

#[derive(Deserialize)]
struct Base {
    parallel: Parallel,
}

#[derive(Deserialize)]
pub(crate) struct CiConfiguration {
    #[serde(rename = ".base")]
    base: Base,
    #[serde(rename = ".default-target")]
    default: String,
}

impl CiConfiguration {
    pub(crate) fn expand(&self) -> Vec<String> {
        let mut r = Vec::new();
        for config in self.base.parallel.matrix.iter() {
            let os = config.os.as_str();
            for tier in config.tier.iter() {
                for version in config.version.iter() {
                    for variant in config.variant.iter() {
                        r.push(format!("{os}-{tier}{variant}-{version}"))
                    }
                }
            }
        }
        r
    }

    pub(crate) fn default(&self) -> &str {
        self.default.as_str()
    }
}

#[test]
fn test_deserialize() {
    const CONF: &str = r###"
.base:
  parallel:
    matrix:
      - TIER:
          - tier-0
          - tier-1
        OS: centos
        VERSION: [stream9]
        VARIANT: ["", "-rt"]
      - TIER:
          - tier-0
          - tier-1
        OS: fedora
        VERSION: [38]
        VARIANT: ["", "-dev"]
.default-target: fedora-tier-1-38
"###;
    let config: CiConfiguration = serde_yaml::from_str(CONF).unwrap();
    let default = config.default();
    assert_eq!(default, "fedora-tier-1-38");
    let variants = config.expand();
    assert_eq!(variants.len(), 8);
    assert!(variants.iter().find(|k| k.as_str() == default).is_some());
}
