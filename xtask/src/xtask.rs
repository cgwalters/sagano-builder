use anyhow::Result;
use fn_error_context::context;
use xshell::{cmd, Shell};

const VENDORPATH: &str = "target/vendor.tar.zstd";

fn main() {
    if let Err(e) = try_main() {
        eprintln!("error: {e:?}");
        std::process::exit(1);
    }
}

#[allow(clippy::type_complexity)]
const TASKS: &[(&str, fn(&Shell) -> Result<()>)] = &[
    ("vendor", vendor),
    ("install", install),
    ("sudoinstall", sudo_install),
];

fn try_main() -> Result<()> {
    let task = std::env::args().nth(1);
    let sh = xshell::Shell::new()?;
    if let Some(cmd) = task.as_deref() {
        let f = TASKS
            .iter()
            .find_map(|(k, f)| (*k == cmd).then_some(*f))
            .unwrap_or(print_help);
        f(&sh)?;
    } else {
        print_help(&sh)?;
    }
    Ok(())
}

#[context("Vendoring")]
fn vendor(sh: &Shell) -> Result<()> {
    let target = VENDORPATH;
    cmd!(
        sh,
        "cargo vendor-filterer --prefix=vendor --format=tar.zstd {target}"
    )
    .run()?;
    Ok(())
}

fn install_impl(sh: &Shell, wrapper: Option<&str>) -> Result<()> {
    let destdir = std::env::var_os("DESTDIR");
    let destdir = destdir.as_ref().and_then(|v| v.to_str()).unwrap_or("/");
    let wrapper = wrapper.unwrap_or("env");
    cmd!(
        sh,
        "{wrapper} install target/release/sagano-builder {destdir}/usr/bin"
    )
    .run()?;
    Ok(())
}

#[context("Installing")]
fn install(sh: &Shell) -> Result<()> {
    cmd!(sh, "cargo build --release").run()?;
    install_impl(sh, None)
}

#[context("Installing")]
fn sudo_install(sh: &Shell) -> Result<()> {
    cmd!(sh, "cargo build --release").run()?;
    install_impl(sh, Some("sudo"))
}

fn print_help(_sh: &Shell) -> Result<()> {
    println!("Tasks:");
    for (name, _) in TASKS {
        println!("  - {name}");
    }
    Ok(())
}
